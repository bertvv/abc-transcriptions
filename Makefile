## Makefile for generating sheet music and MIDI files from tunes in the ABC
# Music Notation.
#
# In order to work, the following commands are needed:
# - abcm2ps (http://moinejf.free.fr/)
# - abc2midi (https://ifdo.ca/~seymour/runabc/top.html)
# - ps2pdf (part of Ghostscript)
# - convert (part of ImageMagick)
# - zip


##---------- Preliminaries ----------------------------------------------------
.POSIX:     # Get reliable POSIX behaviour
.SUFFIXES:  # Clear built-in inference rules

##---------- Variables --------------------------------------------------------

# Convert to MIDI
abc2midi_cmd := abc2midi

# Convert to PS/PDF. The -i option is useful for debugging, the sheet music
# will have red circles around problematic areas.
abc2ps_cmd := abcm2ps -D format -F tunebook.fmt -O = # -i
abc2eps_cmd := abcm2ps -E -D format -F tunebook.fmt -O  # -i
ps2pdf_cmd := ps2pdf
eps2jpg_cmd := convert -density 300 -resample 300x300

# Variables for build targets
sources := $(wildcard *.abc)
ps_files := $(patsubst %.abc,%.ps,$(sources))
eps_files := $(patsubst %.abc,%.eps,$(sources))
pdf_files := $(patsubst %.abc,%.pdf,$(sources))
midi_files := $(patsubst %.abc,%.mid,$(sources))

# Zip with all pdf's/mids/jpgs
output_dir := archives
archive := bertvv-abc-transcriptions
pdf_archive := $(output_dir)/$(archive)-pdf.zip
mid_archive := $(output_dir)/$(archive)-mid.zip
png_archive := $(output_dir)/$(archive)-png.zip
jpg_archive := $(output_dir)/$(archive)-jpg.zip
archives := $(pdf_archive) $(mid_archive) $(png_archive) $(jpg_archive)

##---------- Build targets ----------------------------------------------------

help: ## Show this help message (default)
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

#
# Convert .abc to several output formats: PDF, PNG, JPG, MIDI
#

# Most of the time, we're interested in generating printable PDFs
all: $(pdf_files) ## Synonym for all-pdf

all-pdf: $(pdf_files) ## Generate PDF sheet music for all tunes

all-midi: $(midi_files) ## Generate a MIDI file for all tunes

# EPS is the basis of converting to common graphics formats like JPG and PNG
all-eps: $(eps_files) ## Generate EPS files for all tunes (can be converted to JPG or PNG)

all-jpg: all-eps ## Generate JPG files for all EPS files currently available
	for tune in *.eps; do $(eps2jpg_cmd) "$${tune}" "$${tune%eps}jpg"; done

all-png: all-eps ## Generate PNG files for all EPS files currently available
	for tune in *.eps; do $(eps2jpg_cmd) "$${tune}" -type grayscale "$${tune%eps}png"; done

%.ps: %.abc
	$(abc2ps_cmd) $<

%.pdf: %.ps
	$(ps2pdf_cmd) $<

%.eps: %.abc
	# Generate an .eps for each tune in the .abc source file
	# Resulting file names are of the form ORIG_NAMENNN.eps with ORIG_NAME
	# the original file name without .abc extension and NNN the sequence number
	# of the tune within the file.
	# Unfortunately, this breaks the file dependency checking mechanism of make.
	$(abc2eps_cmd) $@ $<

%.mid: %.abc
	$(abc2midi_cmd) $<

#
# Creating archives of artefacts
#

$(output_dir):
	mkdir -p $@

.PHONY: $(output_dir)

$(pdf_archive): all
	zip $@ *.pdf

$(mid_archive): all-midi
	zip $@ *.mid

$(png_archive): all-png
	zip $@ *.png

$(jpg_archive): all-jpg
	zip $@ *.jpg

dist: $(output_dir) $(archives) ## Create a zip with the generated files (PDF, MIDI, JPG, PNG)

#
# Cleaning up
#

clean: ## Remove all generated files (PDF, MIDI, EPS, PNG, JPG)
	rm -f *.{mid,pdf,jpg,png,eps}

dist-clean: ## Remove all archive files from the output directory
	rm -f $(output_dir)/*

mrproper: clean dist-clean ## Remove all artifacts (clean and dist-clean)

.PHONY: dist-clean clean mrproper
